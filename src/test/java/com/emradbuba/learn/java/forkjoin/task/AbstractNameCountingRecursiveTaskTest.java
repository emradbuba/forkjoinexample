package com.emradbuba.learn.java.forkjoin.task;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.RecursiveTask;

import com.emradbuba.learn.java.forkjoin.processor.RecursiveTasksProcessor;
import com.emradbuba.learn.java.forkjoin.processor.RecursiveTasksProcessorImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public abstract class AbstractNameCountingRecursiveTaskTest {

    protected static final String SEARCHED_NAME = "TheName";
    protected RecursiveTasksProcessor recursiveTasksProcessor;
    protected List<String> givenNamesList = new ArrayList<>();

    @BeforeEach
    public void setUp() {
        recursiveTasksProcessor = new RecursiveTasksProcessorImpl();
        givenNamesList.clear();
    }

    protected abstract AbstractNameCountingRecursiveTask givenCountingTask();

    @Test
    public void shouldReturnZeroIfEmptyNamesList() {
        givenEmptyNamesList();

        Integer result = recursiveTasksProcessor.processRecursiveTask(givenCountingTask());

        assertEquals(0, result);
    }

    @Test
    public void shouldReturnZeroIfSeachedNameNotFound() {
        givenNamesListWithNames("Adam", "Alicja", "Asia", "Czarek");

        Integer result = recursiveTasksProcessor.processRecursiveTask(givenCountingTask());

        assertEquals(0, result);
    }

    @Test
    public void shouldReturnOneIfSeachedNameAppearsOnlyOnce() {
        givenNamesListWithNames("Adam", "Alicja", "Asia", SEARCHED_NAME, "Czarek");

        Integer result = recursiveTasksProcessor.processRecursiveTask(givenCountingTask());

        assertEquals(1, result);
    }

    @Test
    public void shouldReturnThreeIfSearchedNameAppearsThreeTimes() {
        givenNamesListWithNames("Adam", SEARCHED_NAME, "Alicja", "Asia", SEARCHED_NAME, "Czarek", SEARCHED_NAME);

        Integer result = recursiveTasksProcessor.processRecursiveTask(givenCountingTask());

        assertEquals(3, result);
    }

    @Test
    public void shouldReturnGivenListSizeWhenListContainsOnlySearchedNames() {
        givenNamesListWithNames(SEARCHED_NAME, SEARCHED_NAME, SEARCHED_NAME, SEARCHED_NAME, SEARCHED_NAME);

        Integer result = recursiveTasksProcessor.processRecursiveTask(givenCountingTask());

        Integer expectedResult = givenNamesList.size();
        assertEquals(expectedResult, result);
    }

    private void givenEmptyNamesList() {
        // do not add anything
    }

    private void givenNamesListWithNames(String... names) {
        givenNamesList.addAll(Arrays.asList(names));
    }
}