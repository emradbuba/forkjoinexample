package com.emradbuba.learn.java.forkjoin.task;

public class NameCountingRecursiveTaskWithJoinsTest extends AbstractNameCountingRecursiveTaskTest {
    @Override
    protected AbstractNameCountingRecursiveTask givenCountingTask() {
        return new NameCountingRecursiveTaskWithJoins(givenNamesList, SEARCHED_NAME);
    }
}
