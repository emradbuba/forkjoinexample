package com.emradbuba.learn.java.forkjoin.task;

class NameCountingRecursiveTaskWithComputeAndJoinTest extends AbstractNameCountingRecursiveTaskTest {
    @Override
    protected AbstractNameCountingRecursiveTask givenCountingTask() {
        return new NameCountingRecursiveTaskWithComputeAndJoin(givenNamesList, SEARCHED_NAME);
    }
}