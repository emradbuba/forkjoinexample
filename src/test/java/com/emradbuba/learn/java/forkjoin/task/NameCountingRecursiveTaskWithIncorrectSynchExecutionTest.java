package com.emradbuba.learn.java.forkjoin.task;

class NameCountingRecursiveTaskWithIncorrectSynchExecutionTest extends AbstractNameCountingRecursiveTaskTest {

    @Override
    protected AbstractNameCountingRecursiveTask givenCountingTask() {
        return new NameCountingRecursiveTaskWithIncorrectSynchExecution(givenNamesList, SEARCHED_NAME);
    }
}