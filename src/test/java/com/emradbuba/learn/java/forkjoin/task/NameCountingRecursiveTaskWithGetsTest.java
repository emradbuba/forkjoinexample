package com.emradbuba.learn.java.forkjoin.task;

public class NameCountingRecursiveTaskWithGetsTest extends AbstractNameCountingRecursiveTaskTest {
    @Override
    protected AbstractNameCountingRecursiveTask givenCountingTask() {
        return new NameCountingRecursiveTaskWithGets(givenNamesList, SEARCHED_NAME);
    }
}
