package com.emradbuba.learn.java.forkjoin.task;

import java.util.List;
import java.util.concurrent.ExecutionException;

import lombok.extern.java.Log;

@Log
public class NameCountingRecursiveTaskWithGets extends AbstractNameCountingRecursiveTask {

    public NameCountingRecursiveTaskWithGets(List<String> namesList, String searchedName) {
        super(namesList, searchedName);
    }

    @Override
    protected Integer proceedWithSublists(List<String> listLeft, List<String> listRight) {
        NameCountingRecursiveTaskWithGets taskForLeftList = new NameCountingRecursiveTaskWithGets(listLeft, searchedName);
        NameCountingRecursiveTaskWithGets taskForRightList = new NameCountingRecursiveTaskWithGets(listRight, searchedName);
        taskForLeftList.fork();
        taskForRightList.fork();
        try {
            // Using get() instead of join() gives us more flexibility in case of errors
            // and timeouts handling
            return taskForLeftList.get() + taskForRightList.get();

            // Just as an exaple:
            // return taskForLeftList.get(100, TimeUnit.MILLISECONDS) + taskForRightList.get(2, TimeUnit.SECONDS);

        } catch (InterruptedException e) {
            log.severe("Interruption exception while waiting for subtasks");
            return 0;
        } catch (ExecutionException e) {
            log.severe("Execution exception while waiting for subtasks");
            return 0;
        }
    }
}
