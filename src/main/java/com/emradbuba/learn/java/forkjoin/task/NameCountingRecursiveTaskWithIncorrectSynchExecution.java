package com.emradbuba.learn.java.forkjoin.task;

import java.util.List;

import lombok.extern.java.Log;

@Log
public class NameCountingRecursiveTaskWithIncorrectSynchExecution extends AbstractNameCountingRecursiveTask {

    public NameCountingRecursiveTaskWithIncorrectSynchExecution(List<String> namesList, String searchedName) {
        super(namesList, searchedName);
    }

    @Override
    protected Integer proceedWithSublists(List<String> listLeft, List<String> listRight) {
        NameCountingRecursiveTaskWithIncorrectSynchExecution taskForLeftList = new NameCountingRecursiveTaskWithIncorrectSynchExecution(listLeft, searchedName);
        NameCountingRecursiveTaskWithIncorrectSynchExecution taskForRightList = new NameCountingRecursiveTaskWithIncorrectSynchExecution(listRight, searchedName);
        // Here we schedule the rightList processing to a pool (new thread takes care of computations)
        taskForRightList.fork();


        // return taskForLeftList.compute() + taskForRightList.join();
        return taskForRightList.join() + taskForLeftList.compute(); //  <=== INCORRECT, see comment below!!!
        // observe, that execution times in tests are significantly longer

        /*
        NOTE!
        Be aware that if we swap the compute() and join() invocations, we will lose the benefit of parallelism:
        return taskForRightList.join() + taskForLeftList.compute();  <=== INCORRECT!!!

        The code above would first wait for the second thread's result and after it finished, it would
        start processing of the compute() in the current thread (so, we will see a pure sync
        processing instead of async!!)
         */
    }
}
