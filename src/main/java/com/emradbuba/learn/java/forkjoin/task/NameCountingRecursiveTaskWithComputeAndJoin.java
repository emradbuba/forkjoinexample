package com.emradbuba.learn.java.forkjoin.task;

import java.util.List;

import lombok.extern.java.Log;

@Log
public class NameCountingRecursiveTaskWithComputeAndJoin extends AbstractNameCountingRecursiveTask {

    public NameCountingRecursiveTaskWithComputeAndJoin(List<String> namesList, String searchedName) {
        super(namesList, searchedName);
    }

    @Override
    protected Integer proceedWithSublists(List<String> listLeft, List<String> listRight) {
        NameCountingRecursiveTaskWithComputeAndJoin taskForLeftList = new NameCountingRecursiveTaskWithComputeAndJoin(listLeft, searchedName);
        NameCountingRecursiveTaskWithComputeAndJoin taskForRightList = new NameCountingRecursiveTaskWithComputeAndJoin(listRight, searchedName);
        // Here we schedule the rightList processing to a pool (new thread takes care of computations)
        taskForRightList.fork();

        // ... Meanwhile, we perform our computations (compute()) in current thread, and when it ready
        // we get the result (join()) of the right list (possible we'll wait a bit for the result).
        return taskForLeftList.compute() + taskForRightList.join();

        /*
        NOTE!
        Be aware that if we swap the compute() and join() invocations, we will lose the benefit of parallelism:
        return taskForRightList.join() + taskForLeftList.compute();  <=== INCORRECT!!!

        The code above would first wait for the second thread's result and after it finished, it would
        start processing of the compute() in the current thread (so, we will see a pure sync
        processing instead of async!!)
         */
    }
}
