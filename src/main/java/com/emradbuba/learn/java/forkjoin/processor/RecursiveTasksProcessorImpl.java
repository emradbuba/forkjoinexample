package com.emradbuba.learn.java.forkjoin.processor;

import java.time.Instant;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;

import lombok.extern.java.Log;

@Log
public class RecursiveTasksProcessorImpl implements RecursiveTasksProcessor {

    @Override
    public Integer processRecursiveTask(RecursiveTask<Integer> recursiveTask) {
        ForkJoinPool forkJoinPool = new ForkJoinPool(4);
        log.info("Starting processor action ");
        Instant start = Instant.now();
        Integer result = forkJoinPool.invoke(recursiveTask);
        Instant stop = Instant.now();
        log.info("Processor action finished with time: " + (stop.toEpochMilli() - start.toEpochMilli()));
        return result;
    }
}
