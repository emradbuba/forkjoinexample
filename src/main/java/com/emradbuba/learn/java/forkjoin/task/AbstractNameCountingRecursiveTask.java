package com.emradbuba.learn.java.forkjoin.task;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.RecursiveTask;

import lombok.extern.java.Log;

@Log
public abstract class AbstractNameCountingRecursiveTask extends RecursiveTask<Integer> {
    public static final int FAKE_SLEEP_TIME_MILISECS = 400;
    private final List<String> namesList;
    protected final String searchedName;

    public AbstractNameCountingRecursiveTask(List<String> namesList, String searchedName) {
        this.namesList = new ArrayList<>(namesList);
        this.searchedName = searchedName;
    }

    @Override
    protected Integer compute() {
        simulateSecondSleep();
        if (namesList.isEmpty()) {
            return 0;
        }
        if (namesList.size() == 1) {
            return searchedName.equals(namesList.get(0)) ? 1 : 0;
        } else {
            int middleListIndex = namesList.size() / 2;
            List<String> leftList = namesList.subList(0, middleListIndex);
            List<String> rightList = namesList.subList(middleListIndex, namesList.size());
            return proceedWithSublists(leftList, rightList);
        }
    }

    protected abstract Integer proceedWithSublists(List<String> listLeft, List<String> listRight);

    private void simulateSecondSleep() {
        try {
            Thread.sleep(FAKE_SLEEP_TIME_MILISECS);
        } catch (InterruptedException e) {
            log.severe("Interruption error");
        }
    }
}
