package com.emradbuba.learn.java.forkjoin.task;

import java.util.List;

import lombok.extern.java.Log;

@Log
public class NameCountingRecursiveTaskWithJoins extends AbstractNameCountingRecursiveTask {

    public NameCountingRecursiveTaskWithJoins(List<String> namesList, String searchedName) {
        super(namesList, searchedName);
    }

    @Override
    protected Integer proceedWithSublists(List<String> listLeft, List<String> listRight) {
        // Here we create two new tasks...
        NameCountingRecursiveTaskWithJoins taskForLeftList = new NameCountingRecursiveTaskWithJoins(listLeft, searchedName);
        NameCountingRecursiveTaskWithJoins taskForRightList = new NameCountingRecursiveTaskWithJoins(listRight, searchedName);

        // ... we schedule then in pool (so they are being executed in separate threads according to pool's policy)...
        taskForLeftList.fork();
        taskForRightList.fork();

        // ... an we wait for the result
        return taskForLeftList.join() + taskForRightList.join();
    }

    /*
     * In case we had a RecursiveAction instead of RecursiveTask (so no return value would be required),
     * we could also use ForkJoinTask.invokeAll(recursiveAction1, recursiveAction2) instead of using the fork->join
     * approach
     */
}
