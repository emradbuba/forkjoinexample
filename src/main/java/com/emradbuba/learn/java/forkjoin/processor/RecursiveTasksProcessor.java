package com.emradbuba.learn.java.forkjoin.processor;

import java.util.concurrent.RecursiveTask;

public interface RecursiveTasksProcessor {

    Integer processRecursiveTask(RecursiveTask<Integer> task);

}
